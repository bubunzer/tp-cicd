package com.spring.todo.todoapp.configurations.swagger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;

import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    /**
     * The class logger
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(SwaggerConfiguration.class);
    /**
     * The spring boot environment
     */
    private final Environment environment;


    public SwaggerConfiguration(final Environment environment) {
        this.environment = environment;
    }

    /**
     * The Springfow swagger configuration for the API Swagger docs
     *
     * @return The swagger springfox configuration
     */
    @Bean
    public Docket swaggerSpringfoxApiDocket() {
        LOGGER.debug("Starting Swagger");

        Contact contact = new Contact("Telecom-st-etienne", "", "");

        ApiInfo apiInfo = new ApiInfo(
            "Todo App",
            "Learning CI/CD",
            "0.0.1-SNPASHOT",
            null,
            contact,
            null,
            null,
            new ArrayList<>()
        );

        return new Docket(DocumentationType.SWAGGER_2)
            .host(
                String.format(
                    "%s:%s",
                    InetAddress.getLoopbackAddress().getHostName(),
                    environment.getProperty("server.port")
                )
            )
            .apiInfo(apiInfo)
            .groupName("Core")
            .ignoredParameterTypes(ResponseEntity.class)
            .forCodeGeneration(true)
            .useDefaultResponseMessages(false)
            .directModelSubstitute(ByteBuffer.class, String.class)
            .genericModelSubstitutes(ResponseEntity.class)
            .select()
            .paths(regex("/api/.*"))
            .build();
    }
}
